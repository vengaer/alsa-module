#include "alsa_iface.h"
#define _POSIX_C_SOURCE 200809L
#include <alloca.h>
#include <alsa/asoundlib.h>
#include <alsa/mixer.h>

#include <stdio.h>

double alsa_get_volume(char const* restrict card, char const* restrict control) {
    extern int verbose;
    double frac;
    long vol, min, max;
    snd_mixer_t* handle;
    snd_mixer_selem_id_t* sid;
    snd_mixer_elem_t* elem;
    if(verbose) {
        printf("Getting volume\n");
        printf("Opening mixer...\n");
    }

    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);

    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, control);

    elem = snd_mixer_find_selem(handle, sid);
    if(!elem) {
        fprintf(stderr, "Failed to get elem\n");
        snd_mixer_close(handle);
        return -1.0;
    }
    if(verbose) {
        printf("Found mixer element for card '%s', control '%s'\n", card, control);
    }

    if(snd_mixer_selem_get_playback_volume_range(elem, &min, &max) < 0) {
        fprintf(stderr, "Failed to get volume span\n");
        snd_mixer_close(handle);
        return -1.0;
    }
    if(verbose) {
        printf("Volume span: %ld-%ld\n", min, max);
    }

    if(snd_mixer_selem_get_playback_volume(elem, SND_MIXER_SCHN_FRONT_LEFT, &vol) < 0) {
        fprintf(stderr, "Failed to get volume\n");
        snd_mixer_close(handle);
        return -1.0;
    }
    frac = (double) vol / (double) (max - min);
    if(verbose) {
        printf("Current volume: %d%%\n", (int)(100 * frac));
        printf("Closing mixer...\n\n");
    }

    snd_mixer_close(handle);

    return frac;
}

int alsa_playback_get_switch_state(char const* restrict card, char const* restrict scontrol, unsigned index) {
    extern int verbose;
    int value = -1;
    snd_mixer_t* handle;
    snd_mixer_selem_id_t* sid;
    snd_mixer_elem_t* elem;

    if(verbose) {
        printf("Checking switch state\n");
        printf("Opening mixer...\n");
    }
    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);

    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, index);
    snd_mixer_selem_id_set_name(sid, scontrol);

    elem = snd_mixer_find_selem(handle, sid);
    if(!elem) {
        fprintf(stderr, "Failed to get elem\n");
        snd_mixer_close(handle);
        return -1;
    }
    if(verbose) {
        printf("Found mixer element for card '%s', scontrol '%s', index %d\n", card, scontrol, index);
    }

    if(snd_mixer_selem_has_playback_switch(elem)) {
        if(verbose) {
            printf("Found playback switch for mixer element\n");
        }
        if(snd_mixer_selem_get_playback_switch(elem, SND_MIXER_SCHN_MONO, &value) < 0) {
            fprintf(stderr, "Failed to get playback switch\n");
            snd_mixer_close(handle);
            return -1;
        }
        if(verbose) {
            printf("Playback switch is %s\n", value ? "on" : "off");
            printf("Closing mixer...\n\n");
        }
    }
    snd_mixer_close(handle);

    return value;

}

