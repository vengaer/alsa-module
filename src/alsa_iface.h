#ifndef ALSA_IFACE_H
#define ALSA_IFACE_H

/* Get volume percentage. Return negative value on failure */
double alsa_get_volume(char const* restrict card, char const* restrict control);
int alsa_playback_get_switch_state(char const* restrict card, char const* restrict scontrol, unsigned index);

#endif
