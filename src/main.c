#include "alsa_iface.h"
#include "volume_bar.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void print_help(void);
int verbose = 0;
static char const default_card[] = "default";
static char const default_control[] = "SoftMaster";
static char const default_scontrol[] = "IEC958";

int main(int argc, char** argv) {
    int i, playback_on;
    unsigned  scontrol_idx = 1;
    char const*card = NULL, *control = NULL, *scontrol = NULL;
    double vol;

    for(i = 1; i < argc; i++) {
        if(strcmp(argv[i], "--card") == 0) {
            card = argv[++i];
        }
        else if(strcmp(argv[i], "--ctrl") == 0) {
            control = argv[++i];
        }
        else if(strcmp(argv[i], "--sctrl") == 0) {
            scontrol = argv[++i];
        }
        else if(strcmp(argv[i], "--sctrl_idx") == 0) {
            scontrol_idx = atoi(argv[++i]);
        }
        else if(strcmp(argv[i], "--verbose") == 0) {
            verbose = 1;
        }
        else if(strcmp(argv[i], "--help") == 0) {
            print_help();
            return 0;
        }
    }

    if(!card) {
        card = default_card;
        if(verbose) {
            printf("No card specified, using %s\n", card);
        }
    }
    if(!control) {
        control = default_control;
        if(verbose) {
            printf("No control specified, using %s\n", control);
        }
    }
    if(!scontrol) {
        scontrol = default_scontrol;
        if(verbose) {
            printf("No scontrol specified, using %s\n", scontrol);
        }
    }
    
    playback_on = alsa_playback_get_switch_state(card, scontrol, scontrol_idx);
    if(playback_on < 0) {
        return 1;
    }
    vol = alsa_get_volume(card, control);
    if(vol < 0) {
        return 1;
    }
    
    print_bar(vol, !playback_on);
    return 0;
}

static void print_help(void) {
    fprintf(stderr, "Usage: alsa_module [options]...\n");
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "  --card CARD        Specify the PCM interface. Default is '%s'\n", default_card);
    fprintf(stderr, "  --ctrl CTRL        Specify the ALSA control. Default is '%s'\n", default_control);
    fprintf(stderr, "  --sctrl SCTRL      Specify the ALSA scontrol. Default is '%s'\n", default_scontrol);
    fprintf(stderr, "  --sctrl_idx IDX    Specify the ALSA scontrol index. Default is 1\n");
    fprintf(stderr, "  --verbose          Verbose output\n");
    fprintf(stderr, "  --help             Print this help message\n");
}
