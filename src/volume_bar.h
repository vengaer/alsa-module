#ifndef VOLUME_BAR_H
#define VOLUME_BAR_H

#define BAR_WIDTH 10
#define BAR_INDICATOR '|'
#define BAR_FILL '-'

#define MUTED_ICON " "
#define UNMUTED_ICON "  "

void print_bar(double vol, unsigned muted);

#endif
